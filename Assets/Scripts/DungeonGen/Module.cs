﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Module : MonoBehaviour {

    public string[] tags;

    public ModuleGenerator owningGenerator { get; set; }
    public ModuleConnector inputConnector;

    public Module collisionCheckModule;
    public bool checkIsModuleColliding = false;
    public int generatedId;

    public bool checkIsColliding = false;

    public Bounds GetBounds() {
        Renderer[] renderers = GetComponentsInChildren<Renderer>();

        if (renderers.Length <= 0) {
            return new Bounds();
        }

        Bounds b = renderers[0].bounds;

        for (int index = 1; index < renderers.Length; index++) {
            Renderer r = renderers[index];
            b.Encapsulate(r.bounds);
        }

        return b;
    }

    private void OnDrawGizmos() {
        if (!owningGenerator.debugDraw)
            return;
        Gizmos.color = Color.blue;
        Bounds bounds = GetBounds();
        Gizmos.DrawWireCube(bounds.center, bounds.size);
    }

    public void IsColliding() {
        if (owningGenerator) {
            Module[] adjacentModules = owningGenerator.GetAdjacentModules(this);
            bool doesCollide = owningGenerator.DoesCollide(this, adjacentModules);
            Debug.Log("Does collide? " + (doesCollide ? "yes" : "no"));
        }

        checkIsColliding = false;
    }

    private void Update() {
        if(checkIsColliding)
            IsColliding();

        if (checkIsModuleColliding && collisionCheckModule != null)
            IsModuleColliding();
    }

    private void IsModuleColliding() {
        Bounds selfBounds = GetBounds();
        Bounds bounds = collisionCheckModule.GetBounds();

        bool doesCollide = selfBounds.Intersects(bounds);

        Debug.Log("Does collide? " + (doesCollide ? "yes" : "no"));
        checkIsModuleColliding = false;
    }

    public ModuleConnector[] GetExits() {
        return GetComponentsInChildren<ModuleConnector>();
    }

}
