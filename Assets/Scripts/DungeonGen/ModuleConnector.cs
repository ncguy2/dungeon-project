﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModuleConnector : MonoBehaviour {

    public string[] tags;
    public bool isDefault;

    private ModuleConnector connected;
    private Module owningModule;

    public Module GetOwningModule() {
        if (owningModule == null) {
            GameObject parent = GetParent(gameObject);

            while (parent != null) {
                if (parent.TryGetComponent(out Module m)) {
                    owningModule = m;
                    return m;
                }
                parent = GetParent(parent);
            }

            throw new Exception("Module connector is not child of Module");
        }
        return owningModule;
    }

    private static GameObject GetParent(GameObject obj) {
        return obj.GetComponentInParent<Module>()?.gameObject;
    }

    private void OnDrawGizmos() {

        if (!GetOwningModule().owningGenerator.debugDraw)
            return;

        var scale = 1.0f;

        Gizmos.color = Color.blue;
        Gizmos.DrawLine(transform.position, transform.position + transform.forward * scale);

        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.position - transform.right * scale);
        Gizmos.DrawLine(transform.position, transform.position + transform.right * scale);

        Gizmos.color = Color.green;
        Gizmos.DrawLine(transform.position, transform.position + transform.up * scale);

        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(transform.position, 0.125f);
    }

    public void SetConnected(ModuleConnector connector) {
        this.connected = connector;
    }

    public ModuleConnector GetConnected() {
        return connected;
    }
}
