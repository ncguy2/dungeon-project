﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UnityEngine;
using Random = UnityEngine.Random;

public class ModuleGenerator : MonoBehaviour {
    public Module initialModule;
    public List<Module> modules;
    private int nextModuleId = 0;

    public int iterations;

    public bool debugDraw;
    public float debugDrawDelay = 1.0f;

    private List<Module> generatedModules;

    private List<ModuleConnectorNode> graphs;

    private Dictionary<ModuleConnector, ModuleConnectorNode> nodeMap;

    private TempRenderTask task;

    public ModuleGenerator() {
        graphs = new List<ModuleConnectorNode>();
        nodeMap = new Dictionary<ModuleConnector, ModuleConnectorNode>();
        generatedModules = new List<Module>();
    }

    private ModuleConnectorNode GetNode(ModuleConnector connector) {
        if (!nodeMap.ContainsKey(connector)) {
            ModuleConnectorNode node = new ModuleConnectorNode(connector);
            nodeMap.Add(connector, node);
        }

        return nodeMap[connector];
    }

    private List<ModuleConnector> pendingExits;

    // Start is called before the first frame update
    private void Start() {
        Module startModule = Instantiate(initialModule, transform);
        startModule.generatedId = nextModuleId++;
        startModule.owningGenerator = this;
        pendingExits = new List<ModuleConnector>(startModule.GetExits());

        generatedModules.Add(startModule);

        for (int i = 0; i < pendingExits.Count; i++) {
            graphs.Add(GetNode(pendingExits[i]));
        }

    }


    private float time = 0;
    public float interval = 0.1f;
    private int curIteration = 0;

    private bool iterationInProgress = true;
    private int exitIdx = 0;
    List<ModuleConnector> newExits = new List<ModuleConnector>();

    enum States {
        Creating,
        Verifying
    }

    private States state;

    private void NextState() {
        state = state == States.Creating ? States.Verifying : States.Creating;
    }

    private Module lastModule;
    private ModuleConnector lastExit;

    private void Update() {

        if(checkOverlaps)
            CheckAllModulesForOverlaps();;

        time += Time.deltaTime;

        if (task != null) {
            if (task.Tick(Time.deltaTime))
                task = null;
            else
                return;
        }


        if (state == States.Creating && exitIdx >= pendingExits.Count) {
            iterationInProgress = false;
        }

        if (time >= interval) {
            time -= interval;
            if (iterationInProgress) {
                if (curIteration >= iterations) return;

                switch (state) {
                    case States.Creating:
                        lastExit = pendingExits[exitIdx++];
                        if (lastExit != null)
                            lastModule = ProcessExit(lastExit);
                        break;
                    case States.Verifying:
                        VerifyExit(lastModule, lastExit);
                        break;
                }

                CheckAllModulesForOverlaps();
                NextState();
            } else {
                pendingExits = newExits;
                newExits = new List<ModuleConnector>();
                exitIdx = 0;
                iterationInProgress = true;
                curIteration++;
                state = States.Creating;
            }
        }

    }

    private Module ProcessExit(ModuleConnector pendingExit) {
        // Randomly select a tag from the pending exit's
        //   compatible tags
        // string tag = GetRandom(pendingExit.tags);
        // Randomly select a module with said tag, then
        //   create it in the world (anywhere works, location
        //   will be sorted out when the exits are matched
        // Module newModulePrefab = GetRandomWithTag(modules, tag);
        Module newModulePrefab = GetRandom(modules.ToArray());
        Module newModule = Instantiate(newModulePrefab, gameObject.transform);
        newModule.generatedId = nextModuleId++;

        newModule.owningGenerator = this;

        // Get the default exit, or select a random one if none is found
        ModuleConnector[] newModuleExits = newModule.GetExits();
        ModuleConnector exitToMatch = newModuleExits.FirstOrDefault(x => x.isDefault) ?? GetRandom(newModuleExits);
        newModule.inputConnector = exitToMatch;

        // Match the exits
        try {
            MatchExits(pendingExit, exitToMatch);
        } catch (MissingReferenceException mre) {
            Debug.LogWarning("Missing reference exce");
        }

        generatedModules.Add(newModule);

        GetNode(pendingExit).Add(GetNode(exitToMatch));

        newExits.AddRange(newModuleExits.Where(e => e != exitToMatch));

        return newModule;
    }

    private void VerifyExit(Module module, ModuleConnector pendingExit) {
        Module[] adjacentModules = GetAdjacentModules(module);
        if (DoesCollide(module, adjacentModules)) {
            Debug.Log("Module colliding with something");

            pendingExit.GetConnected()?.SetConnected(null);
            pendingExit.SetConnected(null);
            generatedModules.Remove(module);

            ModuleConnector[] deadExits = module.GetExits();
            newExits.RemoveAll(deadExits.Contains);

            Destroy(module.gameObject);
        }
    }

    private List<Module> overlaps = new List<Module>();
    public bool checkOverlaps = false;
    public void CheckAllModulesForOverlaps() {
        overlaps.Clear();
        foreach (Module m in generatedModules.Where(m => DoesCollide(m, GetAdjacentModules(m)))) {
            overlaps.Add(m);
        }
        checkOverlaps = false;
    }

    public Module[] GetAdjacentModules(Module module) {
        List<Module> mods = new List<Module> {module};

        ModuleConnector[] moduleConnectors = module.GetExits();
        foreach (ModuleConnector connector in moduleConnectors) {
            ModuleConnector connected = connector.GetConnected();
            if (connected != null) {
                mods.Add(connected.GetOwningModule());
            }
        }

        return mods.ToArray();
    }



    public bool DoesCollide(Module obj, Module[] ignore) {
        Bounds[] array = this.generatedModules.Where(x => !ignore.Contains(x)).Select(x => x.GetBounds()).ToArray();

        Bounds bounds = obj.GetBounds();
        return array.Any(bounds.Intersects);
    }

    private void MatchExits(ModuleConnector staticExit, ModuleConnector mutableExit) {
        Transform mutableExitTransform = mutableExit.transform;
        Vector3 mutableExitPosition = mutableExitTransform.position;

        Transform mutableModule = mutableExit.GetOwningModule().transform;
        Vector3 forwardVectorToMatch = -staticExit.transform.forward;
        float correctiveRotation = Azimuth(forwardVectorToMatch) - Azimuth(mutableExit.transform.forward);
        mutableModule.RotateAround(mutableExitPosition, Vector3.up, correctiveRotation);
        Vector3 correctiveTranslation = staticExit.transform.position - mutableExitPosition;
        mutableModule.transform.position += correctiveTranslation;

        staticExit.SetConnected(mutableExit);
        mutableExit.SetConnected(staticExit);
    }

    private void OnDrawGizmos() {

        if (!debugDraw) {
            return;
        }

        Gizmos.color = Color.green;
        foreach (Module module in overlaps) {
            Bounds bounds = module.GetBounds();
            Gizmos.DrawWireCube(bounds.center + (Vector3.up * 0.5f), bounds.size);
            Gizmos.DrawLine(bounds.center, module.transform.position);
        }

        Gizmos.color = Color.cyan;
        if (task != null) {
            task.run();
        }


        Gizmos.color = Color.red;

        Vector3 offset = Vector3.up * 1f;

        foreach (KeyValuePair<ModuleConnector,ModuleConnectorNode> pair in nodeMap) {
            ModuleConnectorNode node = pair.Value;

            foreach (ModuleConnectorNode child in node.children) {
                try {
                    Gizmos.DrawLine(node.connector.transform.position + offset, child.connector.transform.position + offset);
                }catch(Exception e) {}
            }
        }
    }

    private static TItem GetRandom<TItem>(TItem[] array) {
        return array[Random.Range(0, array.Length)];
    }


    private static Module GetRandomWithTag(IEnumerable<Module> modules, string tagToMatch) {
        Module[] matchingModules = modules.Where(m => m.tags.Contains(tagToMatch)).ToArray();
        return GetRandom(matchingModules);
    }


    private static float Azimuth(Vector3 vector) {
        return Vector3.Angle(Vector3.forward, vector) * Mathf.Sign(vector.x);
    }

    private class ModuleConnectorNode {
        public ModuleConnector connector;
        public List<ModuleConnectorNode> children;

        public ModuleConnectorNode(ModuleConnector connector) {
            this.connector = connector;
            this.children = new List<ModuleConnectorNode>();
        }

        public void Add(ModuleConnectorNode node) {
            this.children.Add(node);
        }

    }

    private class TempRenderTask {
        public Action task;
        public float timeout;

        public TempRenderTask(Action task, float timeout) {
            this.task = task;
            this.timeout = timeout;
        }

        public void run() {
            this.task();
        }

        public bool Tick(float delta) {
            this.timeout -= delta;
            if (this.timeout <= 0) {
                return true;
            }
            return false;
        }

    }

}